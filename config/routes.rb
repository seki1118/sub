Rails.application.routes.draw do
  get 'users/new'

  get 'static_pages/home'

  get 'static_pages/help'

  root   'static_pages#home'
  get    '/terms_of_service', to: 'users#terms_of_service'
  get    '/signup',           to: 'users#new'
  post   'likes/:micropost_id/create' => 'likes#create'
  post   '/signup',           to: 'users#create'
  get    '/login',            to: 'sessions#new'
  post   '/login',            to: 'sessions#create'
  delete '/logout',           to: 'sessions#destroy'
  resources :users do
    member do
      get :following, :followers, :likes
    end
  end
          
  resources :relationships,       only: [:create, :destroy]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  
  resources :microposts,           only: [:new,:create, :show, :destroy] 
    
  resources :likes,       only: [:create, :destroy]
end
