class Micropost < ApplicationRecord
  belongs_to :user
  has_many :likes, foreign_key: 'micropost_id', dependent: :destroy
  has_many :users, through: :likes
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }
  validate  :picture_size
  has_many :favorites, foreign_key: 'post_id', dependent: :destroy
  has_many :users, through: :favorites

  private

   
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end
end