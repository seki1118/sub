class LikesController < ApplicationController
  before_action :logged_in_user
  
  def create
    micropost = Micropost.find(params[:micropost_id])
    current_user.like(micropost)
    flash[:success] = "Like"
    redirect_to root_url
  end
  def destroy
    micropost = Micropost.find(params[:micropost_id])
    current_user.unlike(micropost)
    flash[:success] = "Unlike"
  end
  
end